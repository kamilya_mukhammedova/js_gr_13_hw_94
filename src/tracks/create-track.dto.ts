export class CreateTrackDto {
  title: string;
  album: string;
  duration: string;
  url: string | null;
}