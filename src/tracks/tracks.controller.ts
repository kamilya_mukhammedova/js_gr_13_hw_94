import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Query } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Track, TrackDocument } from "../schemas/track.schema";
import { Model } from "mongoose";
import { CreateTrackDto } from "./create-track.dto";

@Controller("tracks")
export class TracksController {
  constructor(@InjectModel(Track.name) private trackModel: Model<TrackDocument>) {}

  @Get()
  getAll(@Query() albumId: { [key: string]: string }) {
    if (albumId.album) {
      const id = albumId.album;
      return this.trackModel.find({ album: id });
    }
    return this.trackModel.find();
  }

  @Post()
  async createTrack(@Body() trackDto: CreateTrackDto) {
    if (!trackDto.title || !trackDto.album || !trackDto.duration) {
      throw new HttpException("Title, album and duration are required", HttpStatus.BAD_REQUEST);
    }
    const track = new this.trackModel({
      title: trackDto.title,
      album: trackDto.album,
      duration: trackDto.duration
    });
    await track.save();
    return track;
  }

  @Delete("/:id")
  async removeTrack(@Param("id") trackId: string) {
    await this.trackModel.deleteOne({ _id: trackId });
    return { message: `Track with id ${trackId} has been removed` };
  }
}
