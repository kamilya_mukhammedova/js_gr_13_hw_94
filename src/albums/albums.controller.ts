import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query, UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { Model } from 'mongoose';
import { CreateAlbumDto } from './create-album.dto';
import { Track, TrackDocument } from '../schemas/track.schema';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
  ) {
  }

  @Get()
  getAll(@Query() artistId: { [key: string]: string }) {
    if(artistId.artist) {
      const id = artistId.artist;
      return this.albumModel.find({artist: id});
    }
    return this.albumModel.find();
  }

  @Get('/:id')
  getOne(@Param('id') id: string) {
    return this.albumModel.findOne({_id: id});
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('image', {dest: './public/uploads/albums'})
  )
  async createAlbum(
    @UploadedFile() file: Express.Multer.File,
    @Body() albumDto: CreateAlbumDto
  ) {
    if(!albumDto.title || !albumDto.artist || !albumDto.yearOfIssue) {
      throw new HttpException('Title, artist and year of issue are required', HttpStatus.BAD_REQUEST);
    }
    const album = new this.albumModel({
      title: albumDto.title,
      artist: albumDto.artist,
      yearOfIssue: albumDto.yearOfIssue,
      image: file ? '/uploads/albums/' + file.filename : null,
    });
    await album.save();
    return album;
  }

  @Delete('/:id')
  async removeAlbum(@Param('id') albumId: string) {
    const tracksInAlbum = await this.trackModel.find({album: albumId});
    for (let i = 0; i < tracksInAlbum.length; i++) {
      await this.trackModel.deleteOne({_id: tracksInAlbum[i]._id});
    }
    await this.albumModel.deleteOne({_id: albumId});
    return {message: `Album with id ${albumId} has been removed`};
  }
}
