export class CreateAlbumDto {
  title: string;
  artist: string;
  yearOfIssue: number;
}