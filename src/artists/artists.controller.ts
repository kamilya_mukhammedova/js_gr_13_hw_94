import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Artist, ArtistDocument } from '../schemas/artist.schema';
import { CreateArtistDto } from './create-artist.dto';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { Track, TrackDocument } from '../schemas/track.schema';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('artists')
export class ArtistsController {
  constructor(
    @InjectModel(Artist.name) private artistModel: Model<ArtistDocument>,
    @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
  ) {
  }

  @Get()
  getAll() {
    return this.artistModel.find();
  }

  @Get('/:id')
  getOne(@Param('id') id: string) {
    return this.artistModel.findOne({_id: id});
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('image', {dest: './public/uploads/artists'})
  )
  async create(
    @UploadedFile() file: Express.Multer.File,
    @Body() artistDto: CreateArtistDto
  ) {
    if (!artistDto.name || !artistDto.information) {
      throw new HttpException('Title, artist and year of issue are required', HttpStatus.BAD_REQUEST);
    }
    const artist = new this.artistModel({
      name: artistDto.name,
      information: artistDto.information,
      image: file ? '/uploads/artists/' + file.filename : null,
    });
    await artist.save();
    return artist;
  }

  @Delete('/:id')
  async remove(@Param('id') id: string) {
    const artistAlbums = await this.albumModel.find({artist: id});
    for (let i = 0; i < artistAlbums.length; i++) {
      const tracksInAlbum = await this.trackModel.find({album: artistAlbums[i]._id});
      for (let i = 0; i < tracksInAlbum.length; i++) {
        await this.trackModel.deleteOne({_id: tracksInAlbum[i]._id});
      }
      await this.albumModel.deleteOne({_id: artistAlbums[i]._id});
    }
    await this.artistModel.deleteOne({_id: id});
    return {message: `Artist with id ${id} has been removed`};
  }
}
